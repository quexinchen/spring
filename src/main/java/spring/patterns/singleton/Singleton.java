package spring.patterns.singleton;

/**
 * Created by  Jun on 2017/5/23.
 * 单例设计模式
 */
public class Singleton {
    //默认构造非法私有化
    private Singleton() {
    }

    private static class SingletonHandler {
        private static final Singleton INSTANCE = new Singleton();
    }

    public static final Singleton getInstance() {
        return SingletonHandler.INSTANCE;
    }
}
