package spring.patterns.proxy.jdk;

/**
 * Created by  Jun on 2017/5/24.
 * 出租接口(jdk)
 */
public interface IRent {
    /**
     * 找房子
     */
    void lookForHouse();
}
