package spring.patterns.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by  Jun on 2017/5/24.
 * 中介()
 */
public class IntermediaryJdkProxy implements InvocationHandler {

    IRent target;

    public Object getInstance(IRent target) throws Exception {
        this.target = target;
        Class clazz = target.getClass();
        return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("你好！我是中介，需要找房子的来找我");
        System.out.println("........................开始选房子................................");
        method.invoke(this.target, args);
        System.out.println("选房结束！找到合适的住进去");
        return null;
    }
}
