package spring.patterns.proxy.jdk;

/**
 * Created by  Jun on 2017/5/23.
 * 租客对租房的要求
 */
public class TenantJdk implements IRent {
    public void lookForHouse() {
        System.out.println("地点：杭州市江干区");
        System.out.println("要求：三室一厅一厨一卫");
        System.out.println("租金：四千元");
    }
}
