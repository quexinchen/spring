package spring.patterns.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created by  Jun on 2017/5/23.
 * 中介（cglib）
 */
public class IntermediaryCglibProxy implements MethodInterceptor {

    public Object getInstance(Class clazz) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(this);
        return enhancer.create();
    }

    public Object intercept(Object object, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("你好！我是中介，需要找房子的来找我");
        System.out.println("........................开始选房子................................");
        proxy.invokeSuper(object, args);
        System.out.println("选房结束！找到合适的住进去");
        return null;
    }
}
