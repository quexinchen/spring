package spring.patterns.prototype.simple;

import java.util.ArrayList;

/**
 * Created by  Jun on 2017/5/23.
 * 浅拷贝对象
 */
public class SimplePrototype implements Cloneable {
    private String name;
    private Integer age;

    public ArrayList<String> list = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    protected Object clone() {
        SimplePrototype prototype = null;
        try {
            prototype = (SimplePrototype) super.clone();
            prototype.list = (ArrayList) list.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return prototype;
    }
}
