package spring.patterns.template;

/**
 * Created by  Jun on 2017/5/23.
 * 模板设计模式
 */
public abstract class CookTemplate {
    public final void cook() {
        readyWork();
        originFood();
        addCondiments();
        lastWork();
    }

    public abstract void originFood();

    public abstract void addCondiments();

    private void readyWork() {
        System.out.println("准备好锅、铲、油和调味品");
    }

    private void lastWork() {
        System.out.println("烧好了！准备起锅，准备好盘子！");
    }
}
