package spring.patterns.template;

/**
 * Created by Administrator on 2017/05/30.
 * 西红柿做法
 */
public class Tomato extends CookTemplate {
    @Override
    public void originFood() {
        System.out.println("放入西红柿，准备做西红柿炒鸡蛋！");
    }

    @Override
    public void addCondiments() {
        System.out.println("把打好的鸡蛋放入锅中，加入盐，炒起来！好好吃的样子，都流口水了！");
    }
}
