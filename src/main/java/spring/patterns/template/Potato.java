package spring.patterns.template;

/**
 * Created by Administrator on 2017/05/30.
 * 土豆做法
 */
public class Potato extends CookTemplate {
    @Override
    public void originFood() {
        System.out.println("放入土豆丝，准备做酸辣土豆丝！想想都流口水！");
    }

    @Override
    public void addCondiments() {
        System.out.println("放入青椒丝，加入醋，开始加大火力爆炒！我都流口水了，香气喷鼻！");
    }
}
