package spring.patterns.factory.bikeentity;

/**
 * Created by  Jun on 2017/5/24.
 * 摩拜单车
 */
public class MotorBike implements IBike {

    public Bike getBikeByName() {
        Bike motorBike = new Bike();
        motorBike.setName("MotorBike");
        motorBike.setCash(299.00);
        return motorBike;
    }
}
