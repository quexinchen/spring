package spring.patterns.factory.bikeentity;

/**
 * Created by  Jun on 2017/5/24.
 * 单车属性类
 */
public class Bike {
    private String name;
    private Double cash;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "name='" + name + '\'' +
                ", cash=" + cash +
                '}';
    }
}
