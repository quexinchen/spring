package spring.patterns.factory.bikeentity;

/**
 * Created by  Jun on 2017/5/24.
 * 单车接口
 */
public interface IBike {
    /**
     * @return 返回单车的名称、和单车的押金
     */
    Bike getBikeByName();

}
