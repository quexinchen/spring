package spring.patterns.factory.bikeentity;

/**
 * Created by  Jun on 2017/5/24.
 * ofo单车
 */
public class OfoBike implements IBike {

    public Bike getBikeByName() {
        Bike ofoBike = new Bike();
        ofoBike.setName("OfoBike");
        ofoBike.setCash(99.00);
        return ofoBike;
    }
}
