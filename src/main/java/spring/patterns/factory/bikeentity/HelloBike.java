package spring.patterns.factory.bikeentity;

/**
 * Created by  Jun on 2017/5/24.
 * HelloBike单车
 */
public class HelloBike implements IBike {

    public Bike getBikeByName() {
        Bike helloBike = new Bike();
        helloBike.setName("HelloBike");
        helloBike.setCash(199.00);
        return helloBike;
    }
}
