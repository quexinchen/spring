package spring.patterns.factory.abstractor;

import spring.patterns.factory.bikeentity.HelloBike;
import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.bikeentity.MotorBike;
import spring.patterns.factory.bikeentity.OfoBike;

/**
 * Created by  Jun on 2017/5/23.
 * 抽象工厂
 */
public abstract class AbstractFactory {
    protected abstract IBike getBikeByName();

    IBike getBikeByName(String name) {
        switch (name) {
            case "OfoBike":
                return new OfoBike();
            case "HelloBike":
                return new HelloBike();
            case "MotorBike":
                return new MotorBike();
            default:
                return null;
        }
    }
}
