package spring.patterns.factory.abstractor;

import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.bikeentity.MotorBike;
import spring.patterns.factory.function.BikeFactory;

/**
 * Created by  Jun on 2017/5/24.
 * 摩拜单车工厂
 */
public class MotorFactory extends AbstractFactory {
    @Override
    public IBike getBikeByName() {
        return new MotorBike();
    }
}
