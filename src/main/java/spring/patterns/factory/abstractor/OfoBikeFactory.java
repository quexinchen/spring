package spring.patterns.factory.abstractor;

import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.bikeentity.OfoBike;
import spring.patterns.factory.function.BikeFactory;

/**
 * Created by  Jun on 2017/5/24.
 * Ofo单车工厂
 */
public class OfoBikeFactory extends AbstractFactory {
    @Override
    public IBike getBikeByName() {
        return new OfoBike();
    }
}
