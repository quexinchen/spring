package spring.patterns.factory.abstractor;

import spring.patterns.factory.bikeentity.HelloBike;
import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.function.BikeFactory;

/**
 * Created by  Jun on 2017/5/24.
 * HelloBike工厂
 */
public class HelloBikeFactory extends AbstractFactory {
    @Override
    public IBike getBikeByName() {
        return new HelloBike();
    }
}
