package spring.patterns.factory.abstractor;

import spring.patterns.factory.bikeentity.IBike;

/**
 * Created by  Jun on 2017/5/24.
 * 默认为Ofo工厂
 */
public class DefaultFactory extends AbstractFactory {
    private OfoBikeFactory defaultFactory = new OfoBikeFactory();

    @Override
    protected IBike getBikeByName() {
        return defaultFactory.getBikeByName();
    }
}
