package spring.patterns.factory.simple;

import spring.patterns.factory.bikeentity.HelloBike;
import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.bikeentity.MotorBike;
import spring.patterns.factory.bikeentity.OfoBike;

/**
 * Created by  Jun on 2017/5/23.
 * 简单工厂类(静态工厂)
 */
public class SimpleFactory {
    IBike getName(String name) {
        switch (name) {
            case "HelloBike":
                return new HelloBike();
            case "OfoBike":
                return new OfoBike();
            case "MotorBike":
                return new MotorBike();
            default:
                return null;
        }
    }
}
