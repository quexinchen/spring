package spring.patterns.factory.function;

import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.bikeentity.OfoBike;

/**
 * Created by  Jun on 2017/5/24.
 * Ofo单车工厂
 */
public class OfoBikeFactory implements BikeFactory {
    @Override
    public IBike getBikeByName() {
        return new OfoBike();
    }
}
