package spring.patterns.factory.function;

import spring.patterns.factory.bikeentity.IBike;

/**
 * Created by  Jun on 2017/5/23.
 * 单车工厂
 */
public interface BikeFactory {
    /**
     * @return 返回单车名称和单车押金
     */
    IBike getBikeByName();
}
