package spring.patterns.factory.function;

import spring.patterns.factory.bikeentity.IBike;
import spring.patterns.factory.bikeentity.MotorBike;

/**
 * Created by  Jun on 2017/5/24.
 * 摩拜单车工厂
 */
public class MotorFactory implements BikeFactory {
    @Override
    public IBike getBikeByName() {
        return new MotorBike();
    }
}
