package spring.patterns.factory.function;

import spring.patterns.factory.bikeentity.HelloBike;
import spring.patterns.factory.bikeentity.IBike;

/**
 * Created by  Jun on 2017/5/24.
 * HelloBike工厂
 */
public class HelloBikeFactory implements BikeFactory {
    @Override
    public IBike getBikeByName() {
        return new HelloBike();
    }
}
