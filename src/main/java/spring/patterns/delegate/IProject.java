package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * 项目开发接口
 */
public interface IProject {
    Project developing();
}
