package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * 数据库开发
 */
public class DataBaseDevelop implements IProject {
    @Override
    public Project developing() {
        Project project = new Project();
        project.setDevelopName("数据库开发小组");
        project.setProjectCycle(10);
        project.setProjectNum(2);
        return project;
    }
}
