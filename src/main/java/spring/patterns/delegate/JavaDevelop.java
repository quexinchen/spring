package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * java 后台开发
 */
public class JavaDevelop implements IProject{
    @Override
    public Project developing() {
        Project project = new Project();
        project.setDevelopName("Java开发小组");
        project.setProjectCycle(80);
        project.setProjectNum(10);
        return project;
    }
}
