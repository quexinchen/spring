package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * 移动端开发
 */
public class AppDevelop implements IProject{
    @Override
    public Project developing() {
        Project project = new Project();
        project.setDevelopName("移动开发小组");
        project.setProjectCycle(90);
        project.setProjectNum(5);
        return project;
    }
}
