package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * 前端开发
 */
public class WebDevelop implements IProject {
    @Override
    public Project developing() {
        Project project = new Project();
        project.setDevelopName("前端开发小组");
        project.setProjectCycle(90);
        project.setProjectNum(10);
        return project;
    }
}
