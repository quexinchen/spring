package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * 调度者
 */
public class Dispatcher implements IProject {
    IProject project;

    public Dispatcher(IProject project) {
        this.project = project;
    }

    @Override
    public Project developing() {
        return this.project.developing();
    }
}
