package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/23.
 * 项目开发相关属性
 */
public class Project {
    private String developName;//开发小组
    private Integer projectCycle;//开发周期
    private Integer projectNum;//开发人数

    public String getDevelopName() {
        return developName;
    }

    public void setDevelopName(String developName) {
        this.developName = developName;
    }

    public Integer getProjectCycle() {
        return projectCycle;
    }

    public void setProjectCycle(Integer projectCycle) {
        this.projectCycle = projectCycle;
    }

    public Integer getProjectNum() {
        return projectNum;
    }

    public void setProjectNum(Integer projectNum) {
        this.projectNum = projectNum;
    }

    @Override
    public String toString() {
        return "Project{" +
                "developName='" + developName + '\'' +
                ", projectCycle=" + projectCycle +
                ", projectNum=" + projectNum +
                '}';
    }
}
