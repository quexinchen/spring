package spring.patterns.prototype.simple;

/**
 * Created by Administrator on 2017/05/30.
 * 浅拷贝测试类
 */
public class SimplePrototypeTest {
    public static void main(String[] args) {
        SimplePrototype prototype = new SimplePrototype();
        prototype.setName("沐风");
        prototype.setAge(18);
        prototype.list.add("11111");
        SimplePrototype pt = (SimplePrototype) prototype.clone();
        System.out.println(prototype.list == pt.list);
        System.out.println("原list为：" + prototype.list.toString());
        System.out.println("姓名是：" + pt.getName());
        System.out.println("年龄是：" + pt.getAge());
        System.out.println("list为：" + pt.list.toString());
    }
}
