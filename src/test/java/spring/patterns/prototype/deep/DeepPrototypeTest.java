package spring.patterns.prototype.deep;

import java.util.Date;

/**
 * Created by Administrator on 2017/05/30.
 * 深拷贝测试类
 */
public class DeepPrototypeTest {
    public static void main(String[] args) {
        DeepPrototype prototype = new DeepPrototype();
        prototype.setName("沐风");
        prototype.setAge(18);
        prototype.list.add("11111");
        prototype.setDate(new Date());
        DeepPrototype pt = (DeepPrototype) prototype.clone();
        System.out.println(prototype.list == pt.list);
        System.out.println(prototype.getDate() == pt.getDate());
        System.out.println("原list为：" + prototype.list.toString());
        System.out.println("原Date为：" + prototype.getDate());
        System.out.println("姓名是：" + pt.getName());
        System.out.println("年龄是：" + pt.getAge());
        System.out.println("list为：" + pt.list.toString());
        System.out.println("Date为：" + pt.getDate());
    }
}
