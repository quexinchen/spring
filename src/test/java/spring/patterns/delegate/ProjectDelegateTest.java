package spring.patterns.delegate;

/**
 * Created by  Jun on 2017/5/25.
 * 项目委派测试类
 */
public class ProjectDelegateTest {
    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher(new WebDevelop());
        Project project = dispatcher.developing();
        System.out.println(".....................................................................................");
        System.out.println("开发小组:" + project.getDevelopName() + ",开发周期:" + project.getProjectCycle()
                + ",开发人员数:" + project.getProjectNum());
    }
}
