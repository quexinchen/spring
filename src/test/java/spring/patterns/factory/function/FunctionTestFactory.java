package spring.patterns.factory.function;

import spring.patterns.factory.bikeentity.Bike;
import spring.patterns.factory.bikeentity.IBike;

/**
 * Created by  Jun on 2017/5/24.
 * 工厂方法测试类
 */
public class FunctionTestFactory {
    public static void main(String[] args) {
        BikeFactory bikeFactory = new OfoBikeFactory();
        IBike bike = bikeFactory.getBikeByName();
        Bike nBike = bike.getBikeByName();
        System.out.println("单车为:" + nBike.getName() + ",其押金为:" + nBike.getCash() + "元");
        System.out.println("................................................................................................");
        bikeFactory = new HelloBikeFactory();
        bike = bikeFactory.getBikeByName();
        nBike = bike.getBikeByName();
        System.out.println("单车为:" + nBike.getName() + ",其押金为:" + nBike.getCash() + "元");
        System.out.println("................................................................................................");
        bikeFactory = new MotorFactory();
        bike = bikeFactory.getBikeByName();
        nBike = bike.getBikeByName();
        System.out.println("单车为:" + nBike.getName() + ",其押金为:" + nBike.getCash() + "元");
        System.out.println("................................................................................................");
    }
}
