package spring.patterns.factory.simple;

import spring.patterns.factory.bikeentity.Bike;
import spring.patterns.factory.bikeentity.IBike;

/**
 * Created by  Jun on 2017/5/24.
 * 简单工厂测试类
 */
public class SimpleFactoryTest {
    public static void main(String[] args) {
        IBike bike = new SimpleFactory().getName("OfoBike");
        if (null == bike) {
            System.out.println("输入的单车不存在！");
            return;
        }
        Bike nBike = bike.getBikeByName();
        System.out.println("单车为:" + nBike.getName() + ",其押金为:" + nBike.getCash() + "元");
    }
}
