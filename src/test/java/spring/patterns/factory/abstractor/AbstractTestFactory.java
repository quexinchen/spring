package spring.patterns.factory.abstractor;

import spring.patterns.factory.bikeentity.Bike;
import spring.patterns.factory.bikeentity.IBike;

/**
 * Created by  Jun on 2017/5/24.
 * 抽象工厂测试类
 */
public class AbstractTestFactory {
    public static void main(String[] args) {
        DefaultFactory factory = new DefaultFactory();
        IBike bike = factory.getBikeByName("HelloBike");
        if (null == bike) {
            System.out.println("输入的单车不存在！");
            return;
        }
        Bike nBike = bike.getBikeByName();
        System.out.println("单车为:" + nBike.getName() + ",其押金为:" + nBike.getCash() + "元");
    }
}
