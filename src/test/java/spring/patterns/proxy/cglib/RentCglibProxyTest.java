package spring.patterns.proxy.cglib;

/**
 * Created by  Jun on 2017/5/24.
 * 租房测试
 */
public class RentCglibProxyTest {
    public static void main(String[] args) throws Exception {
        TenantCglib tenantCglib = (TenantCglib) new IntermediaryCglibProxy().getInstance(TenantCglib.class);
        tenantCglib.lookForHouse();
    }
}
