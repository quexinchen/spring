package spring.patterns.proxy.jdk;

/**
 * Created by  Jun on 2017/5/24.
 * 租房测试
 */
public class RentJdkProxyTest {
    public static void main(String[] args) throws Exception {
        IRent target = (IRent) new IntermediaryJdkProxy().getInstance(new TenantJdk());
        target.lookForHouse();
    }
}
