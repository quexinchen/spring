package spring.patterns.template;

/**
 * Created by Administrator on 2017/05/30.
 * 模板测试类
 */
public class CookTemplateTest {
    public static void main(String[] args) {
        //土豆做法
        Potato potato = new Potato();
        potato.cook();
        System.out.println("---------------------------------------------------------");
        //西红柿做法
        Tomato tomato = new Tomato();
        tomato.cook();

    }
}
