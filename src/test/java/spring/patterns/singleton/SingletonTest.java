package spring.patterns.singleton;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2017/05/30.
 * 单例设计模式测试类
 */
public class SingletonTest {
    public static void main(String[] args) {
        Set<Object> hashSet = new HashSet<>();
        int count = 0;
        for (int i = 0; i < 100; i++) {
            Singleton singleton = Singleton.getInstance();
            if (hashSet.add(singleton)) {
                count++;
            }
        }
        System.out.println("创建了" + count + "对象");
    }
}
