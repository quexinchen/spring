<h1>设计模式作业</h1>
<ul class="task-list">
<h2>一、代理设计模式(模拟中介代理租客租房)</h2>
<li>1、jdk代理  测试类: spring.patterns.proxy.jdk.RentJdkProxyTest</li>
<li>2、cglib代理 测试类: spring.patterns.proxy.cglib.RentCglibProxyTest</li>
</ul>

<ul class="task-list">
<h2>二、工厂设计模式(生产共享单车例子)</h2>
<li>1、简单工厂模式 测试类:spring.patterns.factory.simple.SimpleFactoryTest</li>
<li>2、工厂方法模式 测试类:spring.patterns.factory.function.FunctionTestFactory</li>
<li>3、抽象工厂模式 测试类:spring.patterns.factory.abstractor.AbstractTestFactory</li>
</ul>

<ul class="task-list">
<h2>三、委派设计模式(开发项目例子)</h2>
<li>委派设计模式 测试类:spring.patterns.delegate.ProjectDelegateTest</li>
</ul>

<ul class="task-list">
<h2>四、单例设计模式(用Set集合类计算创建多少对象例子)</h2>
<li>单例设计模式 测试类:spring.patterns.singleton.SingletonTest</li>
</ul>

<ul class="task-list">
<h2>五、原型设计模式(浅拷贝用基本数据类型和String类型进行测试;深拷贝用List、Date作为例子测试)</h2>
<li>1、浅拷贝原型设计模式 测试类:spring.patterns.prototype.simple.SimplePrototypeTest</li>
<li>2、深拷贝原型设计模式 测试类:spring.patterns.prototype.deep.DeepPrototypeTest</li>
</ul>

<ul class="task-list">
<h2>六、模板设计模式(用烧菜为例)</h2>
<li>模板设计模式 测试类:spring.patterns.template.CookTemplateTest</li>
</ul>